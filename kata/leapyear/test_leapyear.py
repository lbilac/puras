from leapyear import LeapYear
import unittest


class TestLeapYear(unittest.TestCase):

    def test_should_confirm_that_negative_year_is_not_leap_year(self):
        self.assertEqual(False, LeapYear().leap_year_calculator(-1))

    def test_should_confirm_that_year_higher_than_5000_is_not_leap_year(self):
        self.assertEqual(False, LeapYear().leap_year_calculator(6000))
    
    def test_should_confirm_that_year_divisible_by_4_is_leap_year(self):
        self.assertEqual(True, LeapYear().leap_year_calculator(8))

    def test_should_confirm_that_year_divisible_by_4_and_by_100_is_not_leap_year(self):
        self.assertEqual(False, LeapYear().leap_year_calculator(200))

    def test_should_confirm_that_year_divisible_by_4_by_100_and_by_400_is_leap_year(self):
        self.assertEqual(True, LeapYear().leap_year_calculator(2000))

unittest.main()
