class LeapYear:

    def leap_year_calculator(self, year):
        retVal = False
        if year < 0 or year > 5000 :
            pass
        elif ((year % 4 == 0) and (year % 100 != 0)) or (year % 400 == 0):
            retVal = True
        return retVal