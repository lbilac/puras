# Feature: Leap Year

**As a**: customer <br>
**I want**: to enter an arbitary year <br>
**So that**: I could find if it is a leap year <br>

## Scenario_1:

**Given**: There is a entered year <br>
**When**: year is negative <br>
**Then**: it is not a leap year <br>

## Scenario_2:

**Given**: There is a entered year <br>
**When**: year is higher than 5000 <br>
**Then**: it is not a leap year <br>

## Scenario_3:

**Given**: There is a entered year <br>
**When**: year is divisible by 4 <br>
**Then**: it is a leap year <br>

## Scenario_4:

**Given**: There is a entered year <br>
**When**: year is divisible by 4 and by 100 <br>
**Then**: it is not a leap year <br>

## Scenario_5:

**Given**: There is a entered year <br>
**When**: year is divisible by 4, by 100 and by 400 <br>
**Then**: it is a leap year <br>